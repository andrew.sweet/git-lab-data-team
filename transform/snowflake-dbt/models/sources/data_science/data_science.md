{% docs pte_scores_source %}
Propensity to expand scores created by ML algorithms. 
{% enddocs %}

{% docs ptc_scores_source %}
Propensity to contract and churn scores created by ML algorithms. 
{% enddocs %}
